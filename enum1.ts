enum CardinalDirections {
    North = 1, //กำหนดให้เป็น 1
    East, //-------> 2
    South,//-------> 3
    West
}

let currentDirection = CardinalDirections.North;

console.log(currentDirection);

enum CardinalDirections2 {
    North = "North", 
    East  = "East", 
    South = "South",
    West = "West"
}

let currentDirection2 = CardinalDirections2.North;

console.log(currentDirection2);
